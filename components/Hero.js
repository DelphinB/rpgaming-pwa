import Link from 'next/link';

export function Hero() {
	return (
		<main>
			<div>
				{/* Hero card */}
				<div className="relative">
					<div className="absolute inset-x-0 bottom-0 h-1/2" />
					<div className="mx-auto max-w-7xl sm:px-6 lg:px-8">
						<div className="relative  sm:overflow-hidden sm:rounded-2xl">
							<div className="absolute">
								<div className="absolute inset-0 mix-blend-multiply" />
							</div>
							<div className="relative px-4 py-24 sm:px-6 sm:py-24 lg:py-32 lg:px-8">
								<h1 className="text-center text-4xl font-bold sm:text-5xl lg:text-6xl">
									<span className="block text-[#996487]  -mt-10 lg:mt-0">
										Take control of your
									</span>
									<span className="block text-[#f1e9cf]">customer support</span>
								</h1>
								<div className="mx-auto mt-10 max-w-sm sm:flex sm:max-w-none sm:justify-center">
									<div className="space-y-4 sm:mx-auto sm:inline-grid sm:grid-cols-2 sm:gap-5 sm:space-y-0">
										<a
											href="https://www.eventbrite.fr/"
											className="flex items-center justify-center rounded-md border border-transparent bg-white px-4 py-3 text-base font-medium text-[#996487] shadow-sm hover:bg-indigo-50 sm:px-8"
										>
											Réserver mon billet
										</a>
										<Link href="/programmes/samedi">
											<a className="flex items-center justify-center rounded-md border border-transparent bg-[#996487] bg-opacity-60 px-4 py-3 text-base font-medium text-white shadow-sm hover:bg-opacity-70 sm:px-8">
												Voir le programme du festival
											</a>
										</Link>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
	);
}
