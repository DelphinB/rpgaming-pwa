import { Transition, Dialog } from '@headlessui/react';
import { Fragment, useState } from 'react';

const conf = {
	titre: 'Titre de la conference',
	description:
		'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?',
	horaires: '10h30 - 12h',
};

export function Conf3() {
	const [open, setOpen] = useState(false);
	return (
		<li
			className="relative mt-px flex sm:col-start-4"
			style={{ gridRow: '39 / span 38' }}
		>
			<a
				href="#"
				type="button"
				onClick={() => setOpen(true)}
				className="group absolute inset-1 flex flex-col overflow-y-auto rounded-lg bg-pink-50 p-2 text-xs leading-5 hover:bg-pink-100"
			>
				<p className="order-1 font-semibold text-[#e5262c]">{conf.titre} </p>
				<p className="order-2 text-black truncate">{conf.description}</p>
				<p className="text-pink-500 group-hover:text-[#e5262c]">
					<time dateTime="2022-01-12T07:30">{conf.horaires}</time>
				</p>
			</a>
			{open ? (
				<>
					<Transition.Root show={open} as={Fragment}>
						<Dialog as="div" className="relative z-10" onClose={setOpen}>
							<Transition.Child
								as={Fragment}
								enter="ease-out duration-300"
								enterFrom="opacity-0"
								enterTo="opacity-100"
								leave="ease-in duration-200"
								leaveFrom="opacity-100"
								leaveTo="opacity-0"
							>
								<div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
							</Transition.Child>

							<div className="fixed inset-0 z-10 overflow-y-auto">
								<div className="flex items-end justify-center p-4 text-center sm:items-center sm:p-0">
									<Transition.Child
										as={Fragment}
										enter="ease-out duration-300"
										enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
										enterTo="opacity-100 translate-y-0 sm:scale-100"
										leave="ease-in duration-200"
										leaveFrom="opacity-100 translate-y-0 sm:scale-100"
										leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
									>
										<Dialog.Panel className="relative transform overflow-hidden rounded-lg bg-white px-4 pt-5 pb-4 text-left shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-sm sm:p-6">
											<div>
												<div className="mt-3 text-center sm:mt-5">
													<Dialog.Title
														as="h3"
														className="text-lg font-medium leading-6 text-gray-900"
													>
														{conf.titre}
													</Dialog.Title>
													<div className="mt-2">
														<p className="text-sm text-gray-500">
															{conf.description}
														</p>
													</div>
												</div>
											</div>
											<div className="mt-5 sm:mt-6">
												<button
													type="button"
													className="inline-flex w-full justify-center rounded-md border border-[#996487] hover:text-white px-4 py-2 text-base font-medium text-[#996487] shadow-sm hover:bg-[#996487] sm:text-sm"
													onClick={() => setOpen(false)}
												>
													Fermer
												</button>
											</div>
										</Dialog.Panel>
									</Transition.Child>
								</div>
							</div>
						</Dialog>
					</Transition.Root>
				</>
			) : null}
		</li>
	);
}
