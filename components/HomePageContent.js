export function HomePageContent() {
	return (
		<div className="overflow-hidden">
			<div className="relative mx-auto max-w-7xl py-6">
				<div className="lg:mx-auto max-w-prose text-base lg:grid lg:max-w-none lg:grid-cols-2 lg:gap-8">
					<div>
						<h3 className="mt-2 text-3xl font-bold leading-8 text-[#f1e9cf] sm:text-4xl px-4 sm:px-6 lg:px-8">
							Venez decouvrir le RP Gaming Fest
						</h3>
					</div>
				</div>
				<div className="mt-8 lg:pb-20 lg:grid lg:grid-cols-2 lg:gap-8">
					<div className="relative lg:col-start-2 lg:row-start-1">
						<div className="relative mx-auto text-base lg:max-w-none">
							<figure>
								<div className="aspect-w-12 aspect-h-7 lg:aspect-none">
									<img
										className="object-cover object-center shadow-lg"
										src="/assets/bannière_twitter2-1.png"
										alt="Whitney leaning against a railing on a downtown street"
										width={1184}
										height={1376}
									/>
								</div>
							</figure>
						</div>
					</div>
					<div className="mt-8 lg:mt-0">
						<div className="prose prose-indigo mx-auto mt-5 text-gray-100 lg:col-start-1 lg:row-start-1 lg:max-w-none px-4 sm:px-6 lg:px-8">
							<p>
								Sollicitudin tristique eros erat odio sed vitae, consequat
								turpis elementum. Lorem nibh vel, eget pretium arcu vitae. Eros
								eu viverra donec ut volutpat donec laoreet quam urna.
							</p>
							<p>
								Bibendum eu nulla feugiat justo, elit adipiscing. Ut tristique
								sit nisi lorem pulvinar. Urna, laoreet fusce nibh leo. Dictum et
								et et sit. Faucibus sed non gravida lectus dignissim imperdiet
								a.
							</p>
							<p>
								Dictum magnis risus phasellus vitae quam morbi. Quis lorem lorem
								arcu, metus, egestas netus cursus. In.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}
