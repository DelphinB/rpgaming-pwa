import { Fragment } from 'react';
import { Popover, Transition } from '@headlessui/react';
import { Bars3Icon, XMarkIcon } from '@heroicons/react/24/outline';
import Link from 'next/link';

const navigation = [
	{ name: 'Exposants', href: '/exposants' },
	{ name: 'Programme', href: '/programmes/samedi' },
	{ name: 'Contact', href: '/contact' },
];

export function Navbar() {
	return (
		<div>
			<header>
				<Popover className="relative bg-white">
					<div className="mx-auto flex max-w-7xl items-center justify-between px-4 py-6 sm:px-6 md:justify-start md:space-x-10 lg:px-8">
						<div className="flex justify-start">
							<Link href="/">
								<a>
									<span className="sr-only">Your Company</span>
									<img
										className="h-8 w-auto sm:h-10"
										src="/assets/icon-192x192.png"
										alt=""
									/>
								</a>
							</Link>
						</div>
						<div className="-my-2 -mr-2 md:hidden">
							<Popover.Button className="inline-flex items-center justify-center rounded-md bg-white p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
								<span className="sr-only">Open menu</span>
								<Bars3Icon className="h-6 w-6" aria-hidden="true" />
							</Popover.Button>
						</div>
						<Popover.Group
							as="nav"
							className="hidden space-x-10 md:flex flex-1 justify-center"
						>
							{navigation.map((item) => (
								<Link key={item.name} href={item.href}>
									<a className="font-medium text-gray-500 hover:text-gray-900 nav-list text-xl">
										{item.name}
									</a>
								</Link>
							))}
						</Popover.Group>
					</div>

					<Transition
						as={Fragment}
						enter="duration-200 ease-out"
						enterFrom="opacity-0 scale-95"
						enterTo="opacity-100 scale-100"
						leave="duration-100 ease-in"
						leaveFrom="opacity-100 scale-100"
						leaveTo="opacity-0 scale-95"
					>
						<Popover.Panel
							focus
							className="absolute inset-x-0 top-0 z-30 origin-top-right transform p-2 transition md:hidden"
						>
							<div className="divide-y-2 divide-gray-50 rounded-lg bg-white shadow-lg ring-1 ring-black ring-opacity-5">
								<div className="px-5 pt-5 pb-6">
									<div className="flex items-center justify-between">
										<div>
											<Link href="/">
												<a>
													<img
														className="h-8 w-auto"
														src="/assets/icon-192x192.png"
														alt="Your Company"
													/>
												</a>
											</Link>
										</div>
										<div className="-mr-2">
											<Popover.Button className="inline-flex items-center justify-center rounded-md bg-white p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
												<span className="sr-only">Close menu</span>
												<XMarkIcon className="h-6 w-6" aria-hidden="true" />
											</Popover.Button>
										</div>
									</div>
								</div>
								<div className="pt-2 pb-7 px-5">
									<div className="flex flex-col gap-5">
										{navigation.map((item) => (
											<Link key={item.name} href={item.href}>
												<a className="font-medium text-gray-900 hover:text-gray-700 nav-list text-xl">
													{item.name}
												</a>
											</Link>
										))}
									</div>
								</div>
							</div>
						</Popover.Panel>
					</Transition>
				</Popover>
			</header>
		</div>
	);
}
