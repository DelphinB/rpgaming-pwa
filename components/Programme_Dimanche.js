import { useEffect, useRef } from 'react';

import React from 'react';
import Link from 'next/link';

import { Conf2 } from './Conf2';
import { Conf3 } from './Conf3';

export function Programme() {
	const container = useRef(null);
	const containerNav = useRef(null);
	const containerOffset = useRef(null);

	useEffect(() => {
		// Set the container scroll position based on the current time.
		const currentMinute = new Date().getHours() * 60;
		container.current.scrollTop =
			((container.current.scrollHeight -
				containerNav.current.offsetHeight -
				containerOffset.current.offsetHeight) *
				currentMinute) /
			1440;
	}, []);
	return (
		<div className="flex h-full flex-col lg:max-w-7xl lg:mx-auto">
			<header className="flex flex-none items-center justify-between border-b border-gray-200 py-4 px-6">
				<h1 className="text-xl font-semibold text-gray-900">
					<time dateTime="2023-01">Janvier 2023</time>
				</h1>
			</header>
			<div
				ref={container}
				className="isolate flex flex-auto flex-col overflow-auto bg-white"
			>
				<div
					style={{ width: '165%' }}
					className="flex max-w-full flex-none flex-col sm:max-w-none md:max-w-full"
				>
					<div
						ref={containerNav}
						className="sticky top-0 z-30 flex-none bg-white shadow ring-1 ring-black ring-opacity-5 sm:pr-8"
					>
						<div className="grid grid-cols-7 text-sm leading-6 text-gray-500 sm:hidden">
							<button
								type="button"
								className="flex flex-col items-center pt-2 pb-3"
							>
								J{' '}
								<span className="mt-1 flex h-8 w-8 items-center justify-center font-semibold text-gray-200">
									19
								</span>
							</button>
							<button
								type="button"
								className="flex flex-col items-center pt-2 pb-3"
							>
								V{' '}
								<span className="mt-1 flex h-8 w-8 items-center justify-center font-semibold text-gray-200">
									20
								</span>
							</button>
							<button
								type="button"
								className="flex flex-col items-center pt-2 pb-3"
							>
								S{' '}
								<Link href="/programmes/samedi">
									<span className="mt-1 flex h-8 w-8 items-center justify-center font-semibold text-gray-900 ">
										21
									</span>
								</Link>
							</button>
							<button
								type="button"
								className="flex flex-col items-center pt-2 pb-3"
							>
								D{' '}
								<span className="mt-1 flex h-8 w-8 items-center justify-center rounded-full bg-[#996487] text-white font-semibold ">
									22
								</span>
							</button>
							<button
								type="button"
								className="flex flex-col items-center pt-2 pb-3"
							>
								L{' '}
								<span className="mt-1 flex h-8 w-8 items-center justify-center font-semibold text-gray-200">
									23
								</span>
							</button>
							<button
								type="button"
								className="flex flex-col items-center pt-2 pb-3"
							>
								M{' '}
								<span className="mt-1 flex h-8 w-8 items-center justify-center font-semibold text-gray-200">
									24
								</span>
							</button>
							<button
								type="button"
								className="flex flex-col items-center pt-2 pb-3"
							>
								M{' '}
								<span className="mt-1 flex h-8 w-8 items-center justify-center font-semibold text-gray-200">
									25
								</span>
							</button>
						</div>

						<div className="-mr-px hidden grid-cols-7 divide-x divide-gray-100 border-r border-gray-100 text-sm leading-6 text-gray-200 sm:grid">
							<div className="col-end-1 w-14" />
							<div className="flex items-center justify-center py-3">
								<span>
									Jeu{' '}
									<span className="items-center justify-center font-semibold text-gray-200">
										19
									</span>
								</span>
							</div>
							<div className="flex items-center justify-center py-3">
								<span>
									Ven{' '}
									<span className="items-center justify-center font-semibold text-gray-200">
										20
									</span>
								</span>
							</div>
							<div className="flex items-center justify-center py-3">
								<Link href="/programmes/samedi">
									<span className="text-gray-500">
										Sam{' '}
										<span className="items-center justify-center font-semibold text-gray-900">
											21
										</span>
									</span>
								</Link>
							</div>
							<div className="flex items-center justify-center py-3">
								<span className="flex items-baseline font-bold text-gray-900">
									Dim{' '}
									<span className="ml-1.5 flex h-8 w-8 items-center justify-center rounded-full bg-[#996487] font-semibold text-white">
										22
									</span>
								</span>
							</div>
							<div className="flex items-center justify-center py-3">
								<span>
									Lun{' '}
									<span className="items-center justify-center font-semibold text-gray-200">
										23
									</span>
								</span>
							</div>
							<div className="flex items-center justify-center py-3">
								<span>
									Mar{' '}
									<span className="items-center justify-center font-semibold text-gray-200">
										24
									</span>
								</span>
							</div>
							<div className="flex items-center justify-center py-3">
								<span>
									Mer{' '}
									<span className="items-center justify-center font-semibold text-gray-200">
										25
									</span>
								</span>
							</div>
						</div>
					</div>
					<div className="flex flex-auto">
						<div className="sticky left-0 z-10 w-14 flex-none bg-white ring-1 ring-gray-100" />
						<div className="grid flex-auto grid-cols-1 grid-rows-1">
							{/* Horizontal lines */}
							<div
								className="col-start-1 col-end-2 row-start-1 grid divide-y divide-gray-100"
								style={{ gridTemplateRows: 'repeat(23, minmax(3.5rem, 1fr))' }}
							>
								<div ref={containerOffset} className="row-end-1 h-7"></div>
								<div>
									<div className="sticky left-0 z-20 -mt-2.5 -ml-14 w-14 pr-2 text-right text-xs leading-5 text-gray-400">
										9H
									</div>
								</div>
								<div />
								<div>
									<div className="sticky left-0 z-20 -mt-2.5 -ml-14 w-14 pr-2 text-right text-xs leading-5 text-gray-400">
										10H
									</div>
								</div>
								<div />
								<div>
									<div className="sticky left-0 z-20 -mt-2.5 -ml-14 w-14 pr-2 text-right text-xs leading-5 text-gray-400">
										11H
									</div>
								</div>
								<div />
								<div>
									<div className="sticky left-0 z-20 -mt-2.5 -ml-14 w-14 pr-2 text-right text-xs leading-5 text-gray-400">
										12H
									</div>
								</div>
								<div />
								<div>
									<div className="sticky left-0 z-20 -mt-2.5 -ml-14 w-14 pr-2 text-right text-xs leading-5 text-gray-400">
										13H
									</div>
								</div>
								<div />
								<div>
									<div className="sticky left-0 z-20 -mt-2.5 -ml-14 w-14 pr-2 text-right text-xs leading-5 text-gray-400">
										14H
									</div>
								</div>
								<div />
								<div>
									<div className="sticky left-0 z-20 -mt-2.5 -ml-14 w-14 pr-2 text-right text-xs leading-5 text-gray-400">
										15H
									</div>
								</div>
								<div />
								<div>
									<div className="sticky left-0 z-20 -mt-2.5 -ml-14 w-14 pr-2 text-right text-xs leading-5 text-gray-400">
										16H
									</div>
								</div>
								<div />
								<div>
									<div className="sticky left-0 z-20 -mt-2.5 -ml-14 w-14 pr-2 text-right text-xs leading-5 text-gray-400">
										17H
									</div>
								</div>
								<div />
								<div>
									<div className="sticky left-0 z-20 -mt-2.5 -ml-14 w-14 pr-2 text-right text-xs leading-5 text-gray-400">
										18H
									</div>
								</div>
								<div />
								<div>
									<div className="sticky left-0 z-20 -mt-2.5 -ml-14 w-14 pr-2 text-right text-xs leading-5 text-gray-400">
										19
									</div>
								</div>
								<div />
								<div>
									<div className="sticky left-0 z-20 -mt-2.5 -ml-14 w-14 pr-2 text-right text-xs leading-5 text-gray-400">
										20H
									</div>
								</div>
								<div />
							</div>

							{/* Vertical lines */}
							<div className="col-start-1 col-end-2 row-start-1 hidden grid-cols-7 grid-rows-1 divide-x divide-gray-100 sm:grid sm:grid-cols-7">
								<div className="col-start-1 row-span-full" />
								<div className="col-start-2 row-span-full" />
								<div className="col-start-3 row-span-full" />
								<div className="col-start-4 row-span-full" />
								<div className="col-start-5 row-span-full" />
								<div className="col-start-6 row-span-full" />
								<div className="col-start-7 row-span-full" />
								<div className="col-start-8 row-span-full w-8" />
							</div>

							{/* Events */}
							<ol
								className="col-start-1 col-end-2 row-start-1 grid grid-cols-1 sm:grid-cols-7 sm:pr-8"
								style={{
									gridTemplateRows: '1.75rem repeat(288, minmax(0, 1fr)) auto',
								}}
							>
								<Conf2 />
								<Conf3 />
							</ol>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}
